export default {
  header: {
    search: "Ricerca",
    about: "Chi siamo",
    altFlag: "Bandiera"
  },
  home: {
    latest: "Nuove Ricette",
    top: "Più Piaciute",
    all: "Altre Ricette"
  },
  other_lang: "Altre Lingue",
  recipe: {
    stats: {
      diff: "Difficoltà",
      prep: "Preparazione",
      cook: "Cottura",
      serve: "Porzioni",
      cost: "Costo",
      extra: "Extra"
    },
    diff: {
      Bassa: "Facile",
      Media: "Media",
      Alta: "Difficile"
    },
    cost: {
      Basso: "Basso",
      Medio: "Medio",
      Alto: "Alto"
    },
    ingr: "Ingredienti",
    ofWord: "di",
    qb: "qb",
    prep: "Preparazione",
    rel: "Ricette Correlate"
  },
  search: {
    results: "Risultati per",
    no_res: "Nessun Risultato"
  }
}
