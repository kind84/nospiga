export default {
  header: {
    search: "Search",
    about: "About us",
    altFlag: "Flag"
  },
  home: {
    latest: "Latest Recipes",
    top: "Most Liked",
    all: "Other Recipes"
  },
  other_lang: "Other Languages",
  recipe: {
    stats: {
      diff: "Difficulty",
      prep: "Prep",
      cook: "Cooking",
      serve: "Servings",
      cost: "Cost",
      extra: "Extra"
    },
    diff: {
      Bassa: "Easy",
      Media: "Medium",
      Alta: "Hard"
    },
    cost: {
      Basso: "Low",
      Medio: "Mid",
      Alto: "High"
    },
    ingr: "Ingredients",
    ofWord: "of",
    qb: "to taste",
    prep: "Prep",
    rel: "Related"
  },
  search: {
    results: "Results for",
    no_res: "No Results"
  }
}
