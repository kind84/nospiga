// require('dotenv').config()
// require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` })
require('dotenv').config({ path: `.${process.env.ENV_FILE}.env` })
const ApolloClient = require('apollo-client').ApolloClient
const InMemoryCache = require('apollo-cache-inmemory').InMemoryCache
const HttpLink = require('apollo-link-http').HttpLink
require('isomorphic-unfetch')
const gql = require('graphql-tag')

module.exports = {
  mode: 'universal',

  router: {
    // middleware: 'langDetect'
  },

  head: {
    title: "Cooking with <3",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: "Italian cuisine blog" }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Lato:400,700' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Montserrat:400,700' },
      {
        rel: 'stylesheet',
        href: 'https://use.fontawesome.com/releases/v5.4.2/css/all.css',
        integrity: 'sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns',
        crossorigin: 'anonymous'
      }
    ]
  },

  loading: { color: '#ff7824' },

  css: ['@glidejs/glide/dist/css/glide.core.min.css', '@glidejs/glide/dist/css/glide.theme.min.css'],

  plugins: [
    {
      src: "~/plugins/headroom.js",
      ssr: true
    },
    {
      src: "~/plugins/helper.js",
      ssr: true
    },
    {
      src: "~/plugins/scroll-lock.js",
      ssr: false
    }
  ],

  modules: [
    [
      '@nuxtjs/axios', {
        baseURL: `${process.env.GOSPIGA_URL}`
      }
    ],
    ['@nuxtjs/apollo'],
    [
      'nuxt-i18n',
      {
        locales: [
          { code: 'en', iso: 'en-US', file: 'en.js' },
          { code: 'it', iso: 'it-IT', file: 'it.js' }
        ],
        defaultLocale: 'en',
        detectBrowserLanguage: {
          useCookie: true
        },
        lazy: true,
        langDir: 'lang/',
        seo: false,
        strategy: 'prefix_except_default',
        vueI18n: {
          fallbackLocale: 'en'
        }
      }
    ],
    [
      'nuxt-mq',
      {
        // Default breakpoint for SSR
        defaultBreakpoint: 'default',
        breakpoints: {
          sm: 640,
          md: 768,
          lg: 960,
          xl: Infinity
        }
      }
    ],
    [
      'nuxt-device-detect'
    ]
  ],

  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  apollo: {
    clientConfigs: {
      default: {
        httpEndpoint: `${process.env.DATO_URL}`,
        httpLinkOptions: {
          headers:{
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': `Bearer ${process.env.DATO_TOKEN}`,
          }
        }
      }
    },
    includeNodeModules: true
  },

  server: {
    host: '0.0.0.0'
  },

  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },

  buildModules: [
    // this is required to ensure the rest of Nuxt.js can access the process.env.VAR_NAME
    // '@nuxtjs/dotenv'
    // ['@nuxtjs/dotenv', { filename: `.env.${process.env.NODE_ENV}` }]
    ['@nuxtjs/dotenv', { filename: `.${process.env.ENV_FILE}.env` }]
  ],

  env: {
  },

  generate: {
    routes() {
      let routes = []

      const cache = new InMemoryCache()
      const link = new HttpLink({
        uri: `${process.env.DATO_URL}`,
        headers:{
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': `Bearer ${process.env.DATO_TOKEN}`,
        }
      })

      const client = new ApolloClient({
        cache,
        link
      })
      return client.query({
        query: gql`{
          recipes: allRecipes {
            slug
          }
        }`
      }).then(({ data }) => {
        data.recipes.forEach((recipe) => {
          routes.push(`/recipes/${recipe.slug}`)
        })
        return routes
      })
    }
  }
}
