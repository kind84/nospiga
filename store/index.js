export const state = () => ({
  cacheVersion: '',
  settings: {
    recipe_stats: [],
    suggestions: [],
  },
  version: '',
  searchResults: [],
  slideSuggestions: false,
  steps: null,
  currStep: 0,
  ingredients: []
})

export const mutations = {
  setSettings (state, settings) {
    state.settings = settings
  },
  setCacheVersion (state, version) {
    state.cacheVersion = version
  },
  setVersion (state, version) {
    state.version = version
  },
  setSearchResults (state, results) {
    state.searchResults = results
  },
  setSlideSuggestions (state) {
    state.slideSuggestions = !state.slideSuggestions
  },
  closeSlideSuggestions (state) {
    state.slideSuggestions = false
  },
  setSteps (state, {steps, currStep}) {
    state.steps = steps
    state.currStep = currStep
  },
  setCurrStep (state, currStep) {
    state.currStep = currStep
  },
  setIngredients (state, ingredients) {
    state.ingredients = ingredients
  }
}

export const actions = {
  setVersion ({ commit }, version) {
    commit('setVersion', version)
  },
  setSearchResults ({ commit }, results) {
    commit('setSearchResults', results)
  },
  setSlideSuggestions ({commit}) {
    commit('setSlideSuggestions')
  },
  closeSlideSuggestions ({commit}) {
    commit('closeSlideSuggestions')
  },
  setSteps ({commit}, payload) {
    commit('setSteps', payload)
  },
  setCurrStep ({commit}, currStep) {
    commit('setCurrStep', currStep)
  },
  setIngredients ({commit}, ingredients) {
    commit('setIngredients', ingredients)
  }
}
