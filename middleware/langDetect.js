export default function ({ app, isServer, route, store, isDev }) {
  // inside storyblok editor (_storyblok appended to path) or in dev mode will load draft stories.
  let version = route.query._storyblok || isDev ? 'draft' : 'published'

  store.dispatch('setVersion', version)

  if (isServer) {
    store.commit('setCacheVersion', app.$storyapi.cacheVersion)
  }
}

// export default function ({ app, isServer, route, store, isDev, redirect, req }) {
//   const languages = store.state.languages

//   // inside storyblok editor (_storyblok appended to path) or in dev mode will load draft stories.
//   let version = route.query._storyblok || isDev ? 'draft' : 'published'
//   let language = null

//   store.dispatch('setVersion', version)

//   // retrieve language from path or from request headers. If language not handled, fall back to default.
//   languages.forEach((lang) => {
//     if (route.params.lang === lang) {
//       language = lang
//     }
//   })

//   if (language == null && req) {
//     language = req.headers['accept-language'].match(/[a-zA-Z\-]{2,10}/g).find((lan) => {
//       return languages.includes(lan)
//     })
//   }

//   if (language == null) {
//     language = store.state.language
//   }

//   if (route.path === '/') {
//     redirect('/' + language)
//   }

//   if (isServer) {
//     store.commit('setCacheVersion', app.$storyapi.cacheVersion)
//   }

//   if (!store.state.settings._uid || language !== store.state.language) {
//     store.commit('setLanguage', language)

//     return store.dispatch('loadSettings', {version: version, language: language})
//   }
// }
